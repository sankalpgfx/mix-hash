Mix `crypto` module for easy use. 

# Installation

```js
npm install mix-hash
```

# Example

```js
var Hash = require('mix-hash');
```

md5
```js
Hash.md5('hello');
// return 5d41402abc4b2a76b9719d911017c592
```

sha1
```js
Hash.sha1('hello');
// return aaf4c61ddcc5e8a2dabede0f3b482cd9aea9434d
```

sha256
```js
Hash.sha256('hello'); 
// return 2cf24dba5fb0a30e26e83b2ac5b9e29e1b161e5c1fa7425e73043362938b9824
```

sha512
```js
Hash.sha512('hello'); 
// return 9b71d224bd62f3785d96d46ad3ea3d73319bfbc2890caadae2dff72519673ca72323c3d99ba5c11d7c7acc6e14b8c5da0c4663475c2e5c3adef46f73bcdec043
```

md5 and password
```js
Hash.md5('hello', 'pwd');
// return 5f7916af30bf4b534c5655c97193084a
```

sha1 and password
```js
Hash.sha1('hello', 'pwd');
// return 6e45671e0b52dcdc36f16fe36bb2238e0d5cea18
```

sha256 and password
```js
Hash.sha256('hello', 'pwd');
// return 63627d8b094e1a4e5589f4bf41a0eec538ed354493b6d4ab08a360bd724109ed
```

sha512 and password
```js
Hash.sha512('hello', 'pwd');
// return 3b9cd3e13aec0aa270ed8d23ff38a710da3a53d3fa8ff8e996ba74e208b68e23ec8087a13871f111dab27c77f63fb3f890fc158c9777371128ababd7141f0a99
```

encrypt and decrypt
```js
var e = Hash.encrypt('hello', 'pwd');
// return f4af0c12dca07a10e29bb46b7adc2782
Hash.decrypt(e, 'pwd');
// return hello
```