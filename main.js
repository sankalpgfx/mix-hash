// Invoke 'strict' JavaScript mode
'use strict';
var crypto = require('crypto');

module.exports = {
    _hash: function (s, t, p) {
        if (p && p.length > 0)
            return crypto.createHmac(t, p).update(s, 'utf8').digest('hex');
        else
            return crypto.createHash(t).update(s, 'utf8').digest('hex');
    },

    genId: function () {
        //var args = Array.prototype.slice.call(arguments);
        var args = Array.from(arguments);
        var id = args.join('').toLowerCase();
        return this.md5(id);
    },

    random: function (b) {
        return crypto.randomBytes(b ? b : 8).toString('hex');
    },
    md5: function (s, password) {
        return this._hash(s, 'md5', password);
    },
    sha1: function (s, password) {
        return this._hash(s, 'sha1', password);
    },
    sha256: function (s, password) {
        return this._hash(s, 'sha256', password);
    },
    sha512: function (s, password) {
        return this._hash(s, 'sha512', password);
    },
    pwd: function (s) {
        return this.sha256(this.sha512(s));
    },
    encrypt: function (s, password) {
        //var data = Buffer.from(s, 'utf8');
        var cipher = crypto.createCipher('aes256', password);
        var encrypted = cipher.update(s, 'utf8', 'hex');
        encrypted += cipher.final('hex');
        return encrypted;
    },
    decrypt: function (s, password) {
        var decipher = crypto.createDecipher('aes256', password);
        decipher.setAutoPadding(true);
        var decrypted = decipher.update(s, 'hex', 'utf8');
        decrypted += decipher.final('utf8');
        return decrypted;

    }

}